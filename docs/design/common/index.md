Common {#autoware-common-design}
==========

- @subpage helper-comparisons
- @subpage autoware-common-geometry-design
- @subpage autoware-common-optimization-design
- @subpage autoware-rviz-plugins
- @subpage md_src_prediction_state_estimation_nodes_design_state_estimation_nodes-design
- @subpage md_src_common_covariance_insertion_nodes_design_covariance_insertion_nodes-design
- @subpage mpark_variant_vendor-package-design
